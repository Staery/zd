﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;

namespace Zombie_Defender
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Button[] GoosesBtn;
        private Image[] GoosesImg;

        private BitmapImage BuyImgGoose;
        private BitmapImage GooseLeaderImg;
        private BitmapImage GooseCommonImg;

        public int curGoose
        {
            get
            {
                return CurGoose;
            }
            set
            {
                CurGoose = value;
            }
        }
        private int CurGoose;
      
        public static bool IsOpen = false;
        private static UpgradeForm UP;

        public MainWindow()
        {
            InitializeComponent();
            LbSW.Visibility = Visibility.Hidden; //отладка


        }
        private void ClosingForm(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(UP != null)
                UP.Close();
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            GoosesImg = new Image[100]
            {
                ImgGoose0, ImgGoose1, ImgGoose2, ImgGoose3, ImgGoose4, ImgGoose5, ImgGoose6, ImgGoose7, ImgGoose8, ImgGoose9, ImgGoose10,
                ImgGoose11, ImgGoose12, ImgGoose13, ImgGoose14, ImgGoose15, ImgGoose16, ImgGoose17, ImgGoose18, ImgGoose19, ImgGoose20,
                ImgGoose21, ImgGoose22, ImgGoose23, ImgGoose24, ImgGoose25, ImgGoose26, ImgGoose27, ImgGoose28, ImgGoose29, ImgGoose30,
                ImgGoose31, ImgGoose32, ImgGoose33, ImgGoose34, ImgGoose35, ImgGoose36, ImgGoose37, ImgGoose38, ImgGoose39, ImgGoose40,
                ImgGoose41, ImgGoose42, ImgGoose43, ImgGoose44, ImgGoose45, ImgGoose46, ImgGoose47, ImgGoose48, ImgGoose49, ImgGoose50,
                ImgGoose51, ImgGoose52, ImgGoose53, ImgGoose54, ImgGoose55, ImgGoose56, ImgGoose57, ImgGoose58, ImgGoose59, ImgGoose60,
                ImgGoose61, ImgGoose62, ImgGoose63, ImgGoose64, ImgGoose65, ImgGoose66, ImgGoose67, ImgGoose68, ImgGoose69, ImgGoose70, 
                ImgGoose71, ImgGoose72, ImgGoose73, ImgGoose74, ImgGoose75, ImgGoose76, ImgGoose77, ImgGoose78, ImgGoose79, ImgGoose80,
                ImgGoose81, ImgGoose82, ImgGoose83, ImgGoose84, ImgGoose85, ImgGoose86, ImgGoose87, ImgGoose88, ImgGoose89, ImgGoose90,
                ImgGoose91, ImgGoose92, ImgGoose93, ImgGoose94, ImgGoose95, ImgGoose96, ImgGoose97, ImgGoose98, ImgGoose99, 
            };
            GoosesBtn = new Button[100] 
            { 
                Goose0, Goose1, Goose2, Goose3, Goose4, Goose5, Goose6, Goose7, Goose8, Goose9, Goose10,
                Goose11, Goose12, Goose13, Goose14, Goose15, Goose16, Goose17, Goose18, Goose19, Goose20,
                Goose21, Goose22, Goose23, Goose24, Goose25, Goose26, Goose27, Goose28, Goose29, Goose30,
                Goose31, Goose32, Goose33, Goose34, Goose35, Goose36, Goose37, Goose38, Goose39, Goose40,
                Goose41, Goose42, Goose43, Goose44, Goose45, Goose46, Goose47, Goose48, Goose49, Goose50,
                Goose51, Goose52, Goose53, Goose54, Goose55, Goose56, Goose57, Goose58, Goose59, Goose60,
                Goose61, Goose62, Goose63, Goose64, Goose65, Goose66, Goose67, Goose68, Goose69, Goose70,
                Goose71, Goose72, Goose73, Goose74, Goose75, Goose76, Goose77, Goose78, Goose79, Goose80,
                Goose81, Goose82, Goose83, Goose84, Goose85, Goose86, Goose87, Goose88, Goose89, Goose90,
                Goose91, Goose92, Goose93, Goose94, Goose95, Goose96, Goose97, Goose98, Goose99 
            };
            BuyImgGoose = new BitmapImage(new Uri("Resources/Q8Qm8VJJz7g copy.png", UriKind.Relative));
            GooseLeaderImg = new BitmapImage(new Uri("Resources/GolPWzmgEOc.png", UriKind.Relative));
            GooseCommonImg = new BitmapImage(new Uri("Resources/br8r3-Tycn4.png", UriKind.Relative));

            GoosesImg[0].Source = GooseLeaderImg;
            GoosesImg[1].Source = BuyImgGoose;

            Player.SetMainForm(this);

            this.Visibility = Visibility.Collapsed;
            MainMenu MM = new MainMenu();
            MM.Show();
          
        }
        public void NewGame(bool IsNewGame)
        {
            this.Visibility = Visibility.Visible;
            if (Directory.Exists(Player.FolderZD_)
                && File.Exists(Player.FolderZD_ + Player.GS_)
                && File.Exists(Player.FolderZD_ + Player.PS_)
                && File.Exists(Player.FolderZD_ + Player.RS_)
                && File.Exists(Player.FolderZD_ + Player.ZS_))
            {
                Player.Ахалай_Махалай_Работай_Давай(IsNewGame); 
                LbCountGooses.Content = (Player.GooseCount + 1).ToString();
            }
            else
            { Player.Ахалай_Махалай_Работай_Давай(IsNewGame); }

            for (int i = 0; i < Player.GooseCount; i++) 
            {
                GoosesBtn[i + 1].Visibility = Visibility.Visible;
                GoosesImg[i + 1].Source = GooseCommonImg;
            }
            if (Player.GooseCount < 100) 
            {
                GoosesBtn[Player.GooseCount + 1].Visibility = Visibility.Visible;
                GoosesImg[Player.GooseCount + 1].Source = BuyImgGoose;
            }

            GiveLb("Cost Goose: ", Player.CostBuyGoose.ToString(), LbCostGooses);
        }

        public void GiveLb(string Name, string Num, Label lb) 
        {
            lb.Content = Name + Player.LineSeparation(Num);
        }
        public void GivePG(double MaxHp, double CurHp, ProgressBar pg,Label lb)
        {
            pg.Maximum = MaxHp;
            pg.Value = CurHp;
            lb.Content = Player.LineSeparation(MaxHp.ToString())
                + "/" + Player.LineSeparation(CurHp.ToString());
        }
        public void GiveCurWave(Border BImg, ImageBrush IB) 
        {
            BImg.Background = IB;
        }

        private void Grunes_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 100; i++ )
                if (e.OriginalSource == GoosesBtn[i]) 
                {
                    if (Player.GooseCount + 1 == i)
                    {
                        if (Player.Gall_ >= Player.CostBuyGoose) 
                        {
                            Player.BuyGoose();
                            GoosesBtn[i + 1].Visibility = Visibility.Visible;
                            GoosesImg[i + 1].Source = BuyImgGoose;
                            GoosesImg[i].Source = GooseCommonImg;
                            LbCountGooses.Content = (Player.GooseCount + 1).ToString();

                            GiveLb("Cost Goose: ", Player.CostBuyGoose.ToString(), LbCostGooses);
                        } 
                    }
                    else 
                    {
                        CurGoose = i;
                        if (UP != null)
                            UP.Close();
                        UP = new UpgradeForm(i);
                        UP.Show();
                    }
                    break;
                }                    
        }
    }  
}
