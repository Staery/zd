﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zombie_Defender
{
    /// <summary>
    /// Логика взаимодействия для UpgradeForm.xaml
    /// </summary>
    public partial class UpgradeForm : Window
    {
       
        public UpgradeForm(int СhosenGoose_)
        {
            InitializeComponent();

            ImgRunesSlot[0] = new BitmapImage(new Uri("Resources/icon_ampstone_common.png", UriKind.Relative));
            ImgRunesSlot[1] = new BitmapImage(new Uri("Resources/icon_ampstone_epic.png", UriKind.Relative));
            ImgRunesSlot[2] = new BitmapImage(new Uri("Resources/icon_ampstone_legend.png", UriKind.Relative));

            Player.SetUF(this);
            СhosenGoose = СhosenGoose_;
            CurRuneNum = -66;

            GooseNum.Content = "Goose " + СhosenGoose;

            UpdateStatsG();

            RunesBtn = new Button[100]
            {
                Rune0, Rune1, Rune2, Rune3, Rune4, Rune5, Rune6, Rune7, Rune8, Rune9, Rune10,
                Rune11, Rune12, Rune13, Rune14, Rune15, Rune16, Rune17, Rune18, Rune19, Rune20,
                Rune21, Rune22, Rune23, Rune24, Rune25, Rune26, Rune27, Rune28, Rune29, Rune30,
                Rune31, Rune32, Rune33, Rune34, Rune35, Rune36, Rune37, Rune38, Rune39, Rune40,
                Rune41, Rune42, Rune43, Rune44, Rune45, Rune46, Rune47, Rune48, Rune49, Rune50,
                Rune51, Rune52, Rune53, Rune54, Rune55, Rune56, Rune57, Rune58, Rune59, Rune60,
                Rune61, Rune62, Rune63, Rune64, Rune65, Rune66, Rune67, Rune68, Rune69, Rune70,
                Rune71, Rune72, Rune73, Rune74, Rune75, Rune76, Rune77, Rune78, Rune79, Rune80,
                Rune81, Rune82, Rune83, Rune84, Rune85, Rune86, Rune87, Rune88, Rune89, Rune90, 
                Rune91, Rune92, Rune93, Rune94, Rune95, Rune96, Rune97, Rune98, Rune99
            };
            RunesImg = new Image[100]
            {
                ImgRune0, ImgRune1, ImgRune2, ImgRune3, ImgRune4, ImgRune5, ImgRune6, ImgRune7, ImgRune8, ImgRune9, ImgRune10,
                ImgRune11, ImgRune12, ImgRune13, ImgRune14, ImgRune15, ImgRune16, ImgRune17, ImgRune18, ImgRune19, ImgRune20,
                ImgRune21, ImgRune22, ImgRune23, ImgRune24, ImgRune25, ImgRune26, ImgRune27, ImgRune28, ImgRune29, ImgRune30,
                ImgRune31, ImgRune32, ImgRune33, ImgRune34, ImgRune35, ImgRune36, ImgRune37, ImgRune38, ImgRune39, ImgRune40,
                ImgRune41, ImgRune42, ImgRune43, ImgRune44, ImgRune45, ImgRune46, ImgRune47, ImgRune48, ImgRune49, ImgRune50,
                ImgRune51, ImgRune52, ImgRune53, ImgRune54, ImgRune55, ImgRune56, ImgRune57, ImgRune58, ImgRune59, ImgRune60,
                ImgRune61, ImgRune62, ImgRune63, ImgRune64, ImgRune65, ImgRune66, ImgRune67, ImgRune68, ImgRune69, ImgRune70,
                ImgRune71, ImgRune72, ImgRune73, ImgRune74, ImgRune75, ImgRune76, ImgRune77, ImgRune78, ImgRune79, ImgRune80,
                ImgRune81, ImgRune82, ImgRune83, ImgRune84, ImgRune85, ImgRune86, ImgRune87, ImgRune88, ImgRune89, ImgRune90,
                ImgRune91, ImgRune92, ImgRune93, ImgRune94, ImgRune95, ImgRune96, ImgRune97, ImgRune98, ImgRune99
            };
            RunesGooose = new Button[3]
            {
                GooseRuneSpd, GooseRuneAtk, GooseRuneProfit
            };
            RunesGImg = new Image[3]
            {
                ImgGRune0,ImgGRune1,ImgGRune2
            };

        }
        private void LoadImgRunes(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 100; i++)
                if (Player.StorageRune_[i] != null)
                    RunesImg[i].Source = ImgRunesSlot[Player.StorageRune_[i].Quality];

            for(int i = 0; i < 3;i++)
                if (Player.GooseExistRune(i, Player.CurGoose) == true)
                RunesGImg[i].Source = ImgRunesSlot[Player.GetRuneGoose(i,Player.CurGoose).Quality];
                
            
        }
        private void Closing_Form(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Player.CloseUF();
        }
        private void UpdateStatsG() 
        {
            if (СhosenGoose == 0)
            {
                GooseImg.Source = new BitmapImage(new Uri("Resources/GolPWzmgEOc.png", UriKind.Relative));
                LbSpdG.Content = Player.GetUpdateStat(СhosenGoose, 0).ToString();
                LbAtkG.Content = Player.GetUpdateStat(СhosenGoose, 1).ToString();
                LbProfitG.Content = Player.GetUpdateStat(СhosenGoose, 2).ToString();
            }

            else
            {
                GooseImg.Source = new BitmapImage(new Uri("Resources/br8r3-Tycn4.png", UriKind.Relative));
                LbSpdG.Content = Player.GetUpdateStat(СhosenGoose, 0).ToString();
                LbAtkG.Content = Player.GetUpdateStat(СhosenGoose, 1).ToString();
                LbProfitG.Content = Player.GetUpdateStat(СhosenGoose, 2).ToString();
            }
        }

        private int СhosenGoose;
        private int CurRuneNum;
        private BitmapImage[] ImgRunesSlot = new BitmapImage[3];
        private Button[] RunesGooose;
        private Button[] RunesBtn;
        private Image[] RunesImg;
        private Image[] RunesGImg;
        public void SetImageRune(int NumBtn, int QualityRune) 
        {
            if (NumBtn >= 0)
                RunesImg[NumBtn].Source = ImgRunesSlot[QualityRune];
            else
                RunesGImg[NumBtn].Source = ImgRunesSlot[Math.Abs(QualityRune) + 1 ]; 
        }

        ResultGrind RG;
        private void UpgradeRuneX1_Click(object sender, RoutedEventArgs e)
        {
            UpgradeRune(1);
        }
        private void UpgradeRuneX10_Click(object sender, RoutedEventArgs e)
        {
            UpgradeRune(10);
        }
        private void UpgradeRuneX100_Click(object sender, RoutedEventArgs e)
        {
            UpgradeRune(100);
        }
        private void UpgradeRune(int AttemptsGrind)
        {
            if (CurRuneNum != -66)
            {
                if (CurRuneNum >= 0)
                {
                    if (Player.Gall_ >= Player.StorageRune_[CurRuneNum].CostGrind(AttemptsGrind)) 
                    {
                        int[] RusultG = Player.GrindRune(CurRuneNum, AttemptsGrind);
                        if (RG != null)
                            RG.Close();
                        RG = new ResultGrind(RusultG);
                        RG.Show();

                        LbRuneName.Content = Player.StorageRune_[CurRuneNum].TypeToStr + " руна " + "+" + Player.StorageRune_[CurRuneNum].Grind;
                        LbRuneMainStat.Content = "MainStats: " + Player.StorageRune_[CurRuneNum].MainStat;
                        LbRuneUp1Cost.Content = Player.LineSeparation(Player.StorageRune_[CurRuneNum].CostGrind(1).ToString());
                        LbRuneUp10Cost.Content = Player.LineSeparation(Player.StorageRune_[CurRuneNum].CostGrind(10).ToString());
                        LbRuneUp100Cost.Content = Player.LineSeparation(Player.StorageRune_[CurRuneNum].CostGrind(100).ToString());
                        LbRuneSellCost.Content = Player.LineSeparation(Player.StorageRune_[CurRuneNum].CostSell.ToString());
                    } 
                }
                else if (Player.Gall_ >= Player.GetRuneGoose(Math.Abs(CurRuneNum) - 1, СhosenGoose).CostGrind(AttemptsGrind)) 
                {
                    Rune CurRune = Player.GetRuneGoose(Math.Abs(CurRuneNum) - 1, СhosenGoose);
                    Player.Gall_ -= CurRune.CostGrind(AttemptsGrind);

                    int[] RusultG = CurRune.ChanceGrind(Player.rnd, AttemptsGrind);
                    if (RG != null)
                        RG.Close();
                    RG = new ResultGrind(RusultG);
                    RG.Show();

                    LbRuneName.Content = CurRune.TypeToStr + " руна " + "+" + CurRune.Grind;
                    LbRuneQuality.Content = "Quality: " + CurRune.QualityToStr;
                    LbRuneMainStat.Content = "MainStats: " + CurRune.MainStat;
                    LbRuneUp1Cost.Content = Player.LineSeparation(CurRune.CostGrind(1).ToString());
                    LbRuneUp10Cost.Content = Player.LineSeparation(CurRune.CostGrind(10).ToString());
                    LbRuneUp100Cost.Content = Player.LineSeparation(CurRune.CostGrind(100).ToString());
                    LbRuneSellCost.Content = Player.LineSeparation(CurRune.CostSell.ToString());
                    Player.UpdateStats();
                    UpdateStatsG();
                }
            }
    
        }

        private void EquipRune_Click(object sender, RoutedEventArgs e) 
        {
            if(CurRuneNum != -66)
                if (CurRuneNum >= 0)
                {
                    int TypeRune = Player.StorageRune_[CurRuneNum].Type;
                    int QualityRune = Player.StorageRune_[CurRuneNum].Quality;
                    bool Ok = Player.EquipRune(СhosenGoose, CurRuneNum);
                    if (Ok == true)
                    {
                        RunesImg[CurRuneNum].Source = null;
                        RunesGImg[TypeRune].Source = ImgRunesSlot[QualityRune];
                        ResetSeeStatsRune();
                        UpdateStatsG();
                        CurRuneNum = -66;
                    }
                }
        }
        private void UnEquipRune_Click(object sender, RoutedEventArgs e) 
        {
            if (CurRuneNum != -66)
                if (CurRuneNum < 0)
                {
                    int[] RuneInfo = Player.UnEquipRune(СhosenGoose + 1, Math.Abs(CurRuneNum) - 1);
                    RunesGImg[Math.Abs(CurRuneNum) - 1].Source = null;
                    RunesImg[RuneInfo[0]].Source = ImgRunesSlot[RuneInfo[1]];
                    CurRuneNum = -66;
                    ResetSeeStatsRune();
                    UpdateStatsG();
                }
        } 
        private void SellRune_Click(object sender, RoutedEventArgs e)
        {
            if (CurRuneNum != -66) 
            {
                if (CurRuneNum >= 0)
                {
                    Player.SellRune(CurRuneNum);
                    RunesImg[CurRuneNum].Source = null;
                }
                else
                {
                    Player.SellGRune(CurRuneNum, СhosenGoose);
                    RunesGImg[(Math.Abs(CurRuneNum) -1 )].Source = null;
                    UpdateStatsG();
                }
                CurRuneNum = -66;
                ResetSeeStatsRune();
            }   
        }
        private void SelectedRune_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 100; i++)
                if (e.OriginalSource == RunesBtn[i]) 
                    SeeRune(i);
                  
        }

        private void SeeRune(int Num)
        {
            if (Player.StorageRune_[Num] != null)
            {
                CurRuneNum = Num;
                Rune CurRune = Player.StorageRune_[Num];
                LbRuneName.Content = CurRune.TypeToStr + " Runa " + "+" + CurRune.Grind;
                LbRuneQuality.Content = "Quality: " + CurRune.QualityToStr;
                LbRuneMainStat.Content = "MainStats: " + CurRune.MainStat;
                LbRuneUp1Cost.Content = CurRune.CostGrind(1);
                LbRuneUp10Cost.Content = CurRune.CostGrind(10);
                LbRuneUp100Cost.Content = CurRune.CostGrind(100);
                LbRuneSellCost.Content = CurRune.CostSell;
            }
        }
        private void SeeGooseRune(int TypeRune, int GooseNum)
        {
            Rune CurRune = Player.GetRuneGoose(TypeRune, GooseNum);

            LbRuneName.Content = CurRune.TypeToStr + " Runa " + "+" + CurRune.Grind;
            LbRuneQuality.Content = "Quality: " + CurRune.QualityToStr;
            LbRuneMainStat.Content = "MainStats: " + CurRune.MainStat;
            LbRuneUp1Cost.Content = Player.LineSeparation(CurRune.CostGrind(1).ToString());
            LbRuneUp10Cost.Content = Player.LineSeparation(CurRune.CostGrind(10).ToString());
            LbRuneUp100Cost.Content = Player.LineSeparation(CurRune.CostGrind(100).ToString());
            LbRuneSellCost.Content = Player.LineSeparation(CurRune.CostSell.ToString());
        } 
        private void ResetSeeStatsRune() 
        {
            LbRuneName.Content = "None Runa +0";
            LbRuneMainStat.Content = "MainStats: None";
            LbRuneUp1Cost.Content = "None";
            LbRuneUp10Cost.Content = "None";
            LbRuneUp100Cost.Content = "None";
            LbRuneSellCost.Content = "None";
        }
        
        private int TypeRune;
        private Button DragRuneBtn;
        private BitmapImage DragRuneImg;
        private void Btns_MouseDown(object sender, RoutedEventArgs e) 
        {
            bool IsStorageRune = false;
            Button btn = (Button)sender;

            int NumRune;
            for (NumRune = 0; NumRune < 100; NumRune++)
                if (btn == RunesBtn[NumRune])
                {
                    IsStorageRune = true;
                    break;
                }

            if (IsStorageRune == true)
            {
                if (Player.StorageRune_[NumRune] != null)
                {
                    TypeRune = Player.StorageRune_[NumRune].Type;
                    DragRuneBtn = RunesBtn[NumRune];
                    DragRuneImg = ImgRunesSlot[Player.StorageRune_[NumRune].Quality];
                    SeeRune(NumRune);
                    DragDrop.DoDragDrop(btn, btn, DragDropEffects.Move);
                }
            }
            else
            {
                for (NumRune = -1; NumRune > -4; NumRune--)
                    if (btn == RunesGooose[NumRune + 3])
                        break;

                if (Player.GooseExistRune(NumRune + 3, СhosenGoose))
                {
                    TypeRune = Player.GetRuneGoose(NumRune + 3, СhosenGoose).Type;

                    DragRuneBtn = RunesGooose[NumRune + 3];
                    SeeGooseRune(NumRune + 3, СhosenGoose);
                   
                    DragDrop.DoDragDrop(btn, btn, DragDropEffects.Move);
                }
            }
        }
        private void Btns_Drop(object sender, DragEventArgs e) 
        {
            bool IsStorageRune = false;
            Button btn = (Button)sender;
            if (btn != DragRuneBtn)
            {

                int NumRune;
                for (NumRune = 0; NumRune < 100; NumRune++)
                    if (btn == RunesBtn[NumRune])
                    {
                        IsStorageRune = true;
                        break;
                    }

                if (IsStorageRune)
                {
                    int CurRuneNum_ = CurRuneNum;
                    if (Player.GooseExistRune(TypeRune, СhosenGoose))
                    {
                        switch (TypeRune)
                        {
                            case 0:
                                CurRuneNum = -1;
                                break;
                            case 1:
                                CurRuneNum = -2;
                                break;
                            case 2:
                                CurRuneNum = -3;
                                break;
                        }
                        if (CurRuneNum_ <= 0)
                            UnEquipRune_Click(sender, e);
                    }
                }
                else
                {
                    int EqR = CurRuneNum;
                    if (Player.GooseExistRune(TypeRune, СhosenGoose))
                    {
                        switch (TypeRune)
                        {
                            case 0:
                                CurRuneNum = -1;
                                break;
                            case 1:
                                CurRuneNum = -2;
                                break;
                            case 2:
                                CurRuneNum = -3;
                                break;
                        }
                   
                        UnEquipRune_Click(sender, e);
                        CurRuneNum = EqR;
                        EquipRune_Click(sender, e);
                    }
                    else 
                    {
                        EquipRune_Click(sender, e);
                    }
                }
            }
            // ((Button)sender) = (string)e.Data.GetData(DataFormats.Text);
        }
       
        bool IsNowDragRune = false;
        private void Drag_Mouse() 
        {
            while (IsNowDragRune)
            {
                
            }
        }

        private void GooseRuneProfit_Click(object sender, RoutedEventArgs e)
        {
            if (Player.GooseExistRune(2, СhosenGoose)) 
            {
                SeeGooseRune(2, СhosenGoose);
                CurRuneNum = -3;
            }
        }
        private void GooseRuneAtk_Click(object sender, RoutedEventArgs e)
        {
            if (Player.GooseExistRune(1, СhosenGoose))
            {
                SeeGooseRune(1, СhosenGoose);
                CurRuneNum = -2;
            }
        }
        private void GooseRuneSpd_Click(object sender, RoutedEventArgs e)
        {
            if (Player.GooseExistRune(0, СhosenGoose)) 
            {
                SeeGooseRune(0, СhosenGoose);
                CurRuneNum = -1;
            }
                
        }


        
    }
}
