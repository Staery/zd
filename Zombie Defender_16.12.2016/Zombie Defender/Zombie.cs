﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Defender
{
    class Zombie
    {
        private Zombie_Boss Boss = new Zombie_Boss();

        public int Count_
        {
            get
            {
                return Count;
            }
        }
        private int Count;
        public double HpOne_ 
        {
            get 
            {
                return HpOne;
            }
        }
        private double HpOne;
        public double HpAll_
        {
            get
            {
                return HpAll;
            }
        }
        private double HpAll;
        public double Res_
        {
            get
            {
                return Res;
            }
        }
        double Res;

        private double GoosesDps;
        private double[] ResultStats = new double[2];

        public double GetMaxHp() 
        {
            if(Boss.IsExist_ == true)
                return (((double)Player.Level * ((double)Player.Level / (double)4)) * (double)10000);
            return (10 * HpOne);
        }
        public double GetCurHp()
        {
            if (Boss.IsExist_ == true)
                return Boss.HpBoss_;
            return HpAll;
        }

        public double[] TickWave(double DPS)
        {
            GoosesDps = DPS;

            if (Boss.IsExist_ == true)
                BossWave();
            else
                CommonWave();

            return ResultStats;
        }
        private void BossWave()
        {
            if (Boss.HpBoss_ - GoosesDps <= 0)
            {
                ResultStats[0] = 2;
                ResultStats[1] = ((double)Player.Level * ((double)Player.Level / (double)4)) * (double)300;
                Boss.IsExist_ = false;
                Boss.HpBoss_ = 0;
                NextLvl();
            }
            else 
            {
                ResultStats[0] = -2;
                ResultStats[1] = 0;
                Boss.HpBoss_ -= GoosesDps;
            }
        }
        private void CommonWave() 
        {
            if(HpAll - GoosesDps <= 0)
            {
                ResultStats[0] = 1;
                ResultStats[1] = Count * Res;
                Count = 0;
                HpAll = 0;
                NextLvl();
            }
            else
            {
                
                int LastCount = Count;
                
                HpAll = HpAll - GoosesDps;
                if (HpAll % HpOne == 0)
                    Count = (int)(HpAll / HpOne);
                else
                    Count = (int)(HpAll / HpOne) + 1;

                ResultStats[0] = -1;
                ResultStats[1] = (LastCount - Count) * Res;
            }
        }

        private void NextLvl()
        {
            Player.Level++;
            if (Player.Level % 10 == 0)
                Boss.SpawnBoss();
            else
            {
                Count = 10;
                Res = ((double)Player.Level * ((double)Player.Level / (double)4)) * (double)4;
                HpOne = (int)(((double)Player.Level * ((double)Player.Level / (double)4)) * (double)500);
                HpAll = HpOne * Count;
            }
        }

        public void NewGame() 
        {
            Count = 10;
            Res = ((double)Player.Level * ((double)Player.Level / (double)4)) * (double)4;
            HpOne = (int)(((double)Player.Level * ((double)Player.Level / (double)4)) * (double)500);
            HpAll = HpOne * Count;
        }
        public string GetSave()
        {
            return (Boss.GetSave() + "&=" + Count + "&" + Environment.NewLine);
        }
        public void Loadsave(double HpBoss_, int Count_)
        {
            if (HpBoss_ != -1)
                Boss.LoadBoss(HpBoss_);
            Count = Count_;

            HpOne = (double)(Player.Level * ((double)Player.Level / (double)4)) * (double)500;
            Res = ((double)Player.Level * ((double)Player.Level / (double)4)) * (double)4;
            HpAll = HpOne * Count;
        }
    } 

    class Zombie_Boss
    {
        public bool IsExist_
        {
            get
            {
                return IsExist;
            }
            set 
            {
                IsExist = value;
            }
        }
        private bool IsExist = false;
        public double HpBoss_ 
        {
            get 
            {
                return HpBoss;
            }
            set 
            {
                HpBoss = value;
            }
        }
        private double HpBoss; 

        public void SpawnBoss() 
        {
            IsExist = true;
            HpBoss = ((double)Player.Level * ((double)Player.Level / (double)4)) * (double)10000;
        }
        public void LoadBoss(double HpBoss_) 
        {
            IsExist = true;
            HpBoss = HpBoss_;
        }
        public string GetSave() 
        {
            if(this.IsExist == true)
                return ("&=" + HpBoss);
            return "&=false";
        }
    }
}