﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Defender
{
    public class Rune
    {
        public byte Type; //0 - Spd//1 - Atk//2 - Profit//
        public byte Quality; //0 - Common//1 - Rare//2 - Legendary//
        public int MainStat;
        public int Grind;

        private byte[] SellCost = new byte[3];

        public Rune()
        {
            SellCost[0] = 10;
            SellCost[1] = 30;
            SellCost[2] = 100;
        }
        public Rune(Rune rune)
        {
            SellCost[0] = 10;
            SellCost[1] = 30;
            SellCost[2] = 100;

            Type = rune.Type;
            Quality = rune.Quality;
            MainStat = rune.MainStat;
            Grind = rune.Grind;
            
        }
        public void Generate(Random rnd)
        {
            Type = (byte)rnd.Next(0,3);
            Grind = 0;

            int RndQuality = rnd.Next(0, 101);
            if (RndQuality <= 70)
            {
                Quality = 0;
                MainStat = 5;
            }
            else if (RndQuality <= 95)
            {
                Quality = 1;
                MainStat = 10;
            }
            else 
            {
                Quality = 2;
                MainStat = 20;
            }
        }
        public int[] ChanceGrind(Random rnd, int AttemptsGrind) 
        {
            int[] ResultG = new int[3] { AttemptsGrind, 0, 0 };

            for (int i = 0; i < AttemptsGrind; i++)
            {
                if (((double)rnd.Next(1, 101) + (double)rnd.Next(0, 9999999) / (double)10000000)
                <= ((double)10000 / (((double)Grind + (double)1) + (double)10)))
                {
                    Grind++;
                    switch (Quality)
                    {
                        case 0:
                            MainStat += rnd.Next(1, 3);
                            break;
                        case 1:
                            MainStat += rnd.Next(2, 6);
                            break;
                        case 2:
                            MainStat += rnd.Next(4, 12);
                            break;
                    }
                    ResultG[1]++;
                }
                else
                    ResultG[2]++;
            }
            return ResultG;
        }
        public double CostGrind(int AttemptsGrind)
        {
            double CostG = 0;
            for (int i = 0; i < AttemptsGrind; i++)
                CostG += ((double)(Grind + i) + (double)1) * (double)100 * (double)1.5;

                return Math.Floor(CostG);
        }
        public double Sell //увеличить стоимость продажи
        {
            get
            {
                return Math.Floor(((double)SellCost[Quality] * ((double)Grind + (double)1)));
            }
        }
        public double CostSell
        {
            get
            {
                return Math.Floor(((double)SellCost[Quality] * ((double)Grind + (double)1)));
            }
        }

        public string TypeToStr
        {
            get
            {
                switch (Type)
                {
                    case 0:
                        return "Spd";
                    case 1:
                        return "Atk";
                    case 2:
                        return "Profit";
                }
                return "None";
            }
        }
        public string QualityToStr
        {
           get
           {
               switch (Quality)
               {
                   case 0:
                       return "Common";
                   case 1:
                       return "Epic";
                   case 2:
                       return "Legendary";
               }
               return "None";
           }
        }
    }
}