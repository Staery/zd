﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zombie_Defender
{
    public class Goose
    {
        public int Atk;
        public double AtkSpeed;

        public Rune[] RuneSlot = new Rune[3];

        public void CreateNewGoose() 
        {
            Atk = 1000;//1000
            AtkSpeed = 1;
        }
    }

    class Goose_Leader
    {
        private byte[] LeaderSkill = new byte[3]; //0 - Spd//1 - Atk//2 - Profit//

        private Goose LeaderGoose = new Goose();
        private byte GooseCount;
        public byte GooseCount_ 
        {
            get 
            {
                return GooseCount;
            }
        }
        private Goose[] CommonGoose = new Goose[100];

        private double AllAtk;
        private double AllSpd;
        public double Dps_
        {
            get
            {
                return Dps;
            }
        }
        private double Dps;
        public double AllProfit_
        {
            get
            {
                return AllProfit;
            }
        }
        private double AllProfit;


        public bool IsRuneExist(int GooseNum,int TypeRune) 
        {
            if (GooseNum == 0) 
            {
                if (LeaderGoose.RuneSlot[TypeRune] != null)
                    return true;
            }            
            else if (CommonGoose[GooseNum - 1] != null)
                if (CommonGoose[GooseNum - 1].RuneSlot[TypeRune] != null)
                        return true;

            return false;
        }
        public void EquipRune(Rune rune, int GooseNum) 
        {
            if (GooseNum == 0)
                LeaderGoose.RuneSlot[rune.Type] = new Rune(rune);
            else 
                CommonGoose[GooseNum - 1].RuneSlot[rune.Type] = new Rune(rune);
            UpdateStats();
        }
        public Rune GetRune(int TypeRune, int GooseNum) 
        {
            if (GooseNum == 0)
                return LeaderGoose.RuneSlot[TypeRune];
            return CommonGoose[GooseNum - 1].RuneSlot[TypeRune];
        } 
        public double SellGooseRune(int RuneSlot, int CurGoose) 
        {
            if (CurGoose == 0)
            {
                UpdateStats();
                double SellGall = LeaderGoose.RuneSlot[RuneSlot].Sell;
                LeaderGoose.RuneSlot[RuneSlot] = null;
                return SellGall;
            }
            else 
            {
                UpdateStats();
                double SellGall = CommonGoose[CurGoose - 1].RuneSlot[RuneSlot].Sell;
                CommonGoose[CurGoose - 1].RuneSlot[RuneSlot] = null;
                return SellGall;
            }
                
        }
        public Rune UnEquipGRune(int CurGoose, int CurRune) 
        {
            int CurGoose_ = CurGoose -1;
            Rune rune;
            if (CurGoose_ == 0)
            {
                rune = new Rune(LeaderGoose.RuneSlot[CurRune]);
                LeaderGoose.RuneSlot[CurRune] = null;
            }
            else 
            {
                rune = new Rune(CommonGoose[CurGoose_ - 1].RuneSlot[CurRune]);
                CommonGoose[CurGoose_ - 1].RuneSlot[CurRune] = null;
            }
            UpdateStats();
            return rune;
        }

        public double CostBuyGoose 
        {
            get 
            {
                return Math.Pow((double)GooseCount + (double)1, (double)10) + 1000;
            }
        }
        public double BuyNewGoose() 
        {
            double CostBuyGoose_ = CostBuyGoose;
            CommonGoose[GooseCount] = new Goose();
            CommonGoose[GooseCount].CreateNewGoose();
            GooseCount++;

            UpdateStats();

            return CostBuyGoose_;
        }
        public Goose GetGoose(int GooseNum) 
        {
            if (GooseNum == 0)
                return LeaderGoose;
            else
                return CommonGoose[GooseNum - 1];
        }

        public double UpdateStatsOne(int GooseNum, int TypeUpdate) 
        {
            if (GooseNum == 0)
            {
                switch (TypeUpdate)
                {
                    case 0:
                        if (LeaderGoose.RuneSlot[0] != null)
                            return LeaderGoose.AtkSpeed + ((LeaderGoose.AtkSpeed / 100)
                                    * (LeaderGoose.RuneSlot[0].MainStat + LeaderSkill[0]));
                        return LeaderGoose.AtkSpeed;
                    case 1:
                        if (LeaderGoose.RuneSlot[1] != null)
                            return LeaderGoose.Atk + ((LeaderGoose.Atk / 100)
                                        * (LeaderGoose.RuneSlot[1].MainStat + LeaderSkill[1]));
                        return LeaderGoose.Atk;
                    case 2:
                        if (LeaderGoose.RuneSlot[2] != null)
                            return (double)(LeaderGoose.RuneSlot[2].MainStat)
                                + ((((double)LeaderGoose.RuneSlot[2].MainStat / (double)100) * (double)LeaderSkill[2]));
                        return 0;
                }
            }
            else
            {
                int GooseNum_ = (GooseNum -1);
                switch (TypeUpdate)
                {
                    case 0:
                        if (CommonGoose[GooseNum_].RuneSlot[0] != null)
                            return CommonGoose[GooseNum_].AtkSpeed + ((CommonGoose[GooseNum_].AtkSpeed / 100)
                                    * (CommonGoose[GooseNum_].RuneSlot[0].MainStat + LeaderSkill[0]));
                        return CommonGoose[GooseNum_].AtkSpeed;
                    case 1:
                        if (CommonGoose[GooseNum_].RuneSlot[1] != null)
                            return CommonGoose[GooseNum_].Atk + ((CommonGoose[GooseNum_].Atk / 100)
                        * (CommonGoose[GooseNum_].RuneSlot[1].MainStat + LeaderSkill[1]));
                        return CommonGoose[GooseNum_].Atk;
                    case 2:
                        if (CommonGoose[GooseNum_].RuneSlot[2] != null)
                            return (double)(CommonGoose[GooseNum_].RuneSlot[2].MainStat)
                    + ((((double)CommonGoose[GooseNum_].RuneSlot[2].MainStat / (double)100) * (double)LeaderSkill[2]));
                        return 0;
                }
            
            }
            
            return 0;
        }
        public void UpdateStats() //В зависимости от одетой/снятой руны пересчитывать нужные статы. //см ниже
        {                         //В случае отсутствия ферлольки у ивашки, Нулевая не насосоривает Ивашке// наэлинить эту ферарьку
            if (LeaderGoose.RuneSlot[0] != null)
                AllSpd = LeaderGoose.AtkSpeed + ((LeaderGoose.AtkSpeed / 100)
                        * (LeaderGoose.RuneSlot[0].MainStat + LeaderSkill[0]));
            else
                AllSpd = LeaderGoose.AtkSpeed;

            if (LeaderGoose.RuneSlot[1] != null)
                AllAtk = LeaderGoose.Atk + ((LeaderGoose.Atk / 100)
                            * (LeaderGoose.RuneSlot[1].MainStat + LeaderSkill[1]));
            else
                AllAtk = LeaderGoose.Atk;

            if (LeaderGoose.RuneSlot[2] != null)
                AllProfit = (double)(LeaderGoose.RuneSlot[2].MainStat)
                    + ((((double)LeaderGoose.RuneSlot[2].MainStat / (double)100) * (double)LeaderSkill[2]));
            else
                AllProfit = 0;
            
            for (int i = 0; i < GooseCount; i++)
            {
                if (CommonGoose[i].RuneSlot[0] != null)
                    AllSpd += CommonGoose[i].AtkSpeed + ((CommonGoose[i].AtkSpeed / 100)
                        * (CommonGoose[i].RuneSlot[0].MainStat + LeaderSkill[0]));
                else
                    AllSpd += CommonGoose[i].AtkSpeed;

                if (CommonGoose[i].RuneSlot[1] != null)
                    AllAtk += CommonGoose[i].Atk + ((CommonGoose[i].Atk / 100)
                        * (CommonGoose[i].RuneSlot[1].MainStat + LeaderSkill[1]));
                else
                    AllAtk += CommonGoose[i].Atk;

                if (CommonGoose[i].RuneSlot[2] != null)
                    AllProfit += (double)(CommonGoose[i].RuneSlot[2].MainStat)
                    + ((((double)CommonGoose[i].RuneSlot[2].MainStat / (double)100) * (double)LeaderSkill[2]));
                else
                    AllProfit += 0;
            }

            AllProfit = Math.Floor(AllProfit / 10);
            Dps = Math.Floor(((double)AllAtk * (double)AllSpd) * (double)(GooseCount + 1));
        }
        public void NewGame()
        {
            GooseCount = 0;

            LeaderGoose.CreateNewGoose();
            UpdateStats();
        }
        public void LoadSave(StreamReader sr) 
        {
            string Str = sr.ReadLine();
            for (int i = 0; i < 3; i++)
                LeaderSkill[i] = Byte.Parse(Str.Split('&')[(i + 1)].Split('=')[1]);

            Str = sr.ReadLine();
            LeaderGoose.Atk = Int32.Parse(Str.Split('&')[1].Split('=')[1]);
            LeaderGoose.AtkSpeed = Int32.Parse(Str.Split('&')[2].Split('=')[1]);

            
            for (int i = 0; i < 3; i++) 
            {
                Str = sr.ReadLine();
                if (Str.Split('&')[1].Split('=')[1] != "false")
                {
                    Rune rune = new Rune();
                    rune.Type = Byte.Parse(Str.Split('&')[1].Split('=')[1]);
                    rune.Quality = Byte.Parse(Str.Split('&')[2].Split('=')[1]);
                    rune.MainStat = Int32.Parse(Str.Split('&')[3].Split('=')[1]);
                    rune.Grind = Int32.Parse(Str.Split('&')[4].Split('=')[1]);

                    LeaderGoose.RuneSlot[i] = rune;
                }
            }  

            Str = sr.ReadLine();
            GooseCount = Byte.Parse(Str.Split('&')[1].Split('=')[1]);
            for (int i = 0; i < GooseCount; i++)
            {
                Str = sr.ReadLine();

                CommonGoose[i] = new Goose();
                CommonGoose[i].Atk = Int32.Parse(Str.Split('&')[1].Split('=')[1]);
                CommonGoose[i].AtkSpeed = Double.Parse(Str.Split('&')[2].Split('=')[1]);
               

                for (int j = 0; j < 3; j++)
                {
                    Str = sr.ReadLine();
                    if (Str.Split('&')[1].Split('=')[1] != "false")
                    {
                        CommonGoose[i].RuneSlot[j] = new Rune();
                        CommonGoose[i].RuneSlot[j].Type = Byte.Parse(Str.Split('&')[1].Split('=')[1]);
                        CommonGoose[i].RuneSlot[j].Quality = Byte.Parse(Str.Split('&')[2].Split('=')[1]);
                        CommonGoose[i].RuneSlot[j].MainStat = Int32.Parse(Str.Split('&')[3].Split('=')[1]);
                        CommonGoose[i].RuneSlot[j].Grind = Int32.Parse(Str.Split('&')[4].Split('=')[1]);
                    }
                }
            }

            UpdateStats(); 
        }
        public string GetSave() 
        {
            string Buff = "";

            for (int i = 0; i < 3; i++)
                Buff += "&=" + LeaderSkill[i];
            Buff += "&" + Environment.NewLine;

            Buff += "&=" + LeaderGoose.Atk
                + "&=" + LeaderGoose.AtkSpeed + "&"
                + Environment.NewLine;
            for (int j = 0; j < 3; j++)
                if (LeaderGoose.RuneSlot[j] != null) 
                {
                        Buff += "&=" + LeaderGoose.RuneSlot[j].Type
                                        + "&=" + LeaderGoose.RuneSlot[j].Quality
                                        + "&=" + LeaderGoose.RuneSlot[j].MainStat
                                        + "&=" + LeaderGoose.RuneSlot[j].Grind + "&"
                                        + Environment.NewLine;
                }
                else
                    Buff += "&=false&" + Environment.NewLine;

            Buff += "&=" + GooseCount + "&" + Environment.NewLine; 
            for (int i = 0; i < GooseCount; i++)
            {
                if (CommonGoose[i] != null)
                    Buff += "&=" + CommonGoose[i].Atk
                        + "&=" + CommonGoose[i].AtkSpeed + "&"
                        + Environment.NewLine;
                for (int j = 0; j < 3; j++)
                    if (CommonGoose[i].RuneSlot[j] != null)
                        Buff += "&=" + CommonGoose[i].RuneSlot[j].Type
                        + "&=" + CommonGoose[i].RuneSlot[j].Quality
                        + "&=" + CommonGoose[i].RuneSlot[j].MainStat
                        + "&=" + CommonGoose[i].RuneSlot[j].Grind + "&"
                        + Environment.NewLine;
                    else
                        Buff += "&=false&" + Environment.NewLine;
            }
            return Buff;
        }
    }
}