﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Zombie_Defender
{
    /// <summary>
    /// Логика взаимодействия для MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Window 
    {
        public MainMenu()
        {
            InitializeComponent(); 
        }
        private void ClosedForm(object sender, EventArgs e)
        {
            if(Player.GameIsStarted_ == false)
                Player.MW_.Close();
        }

        private void NewGame_Click(object sender, RoutedEventArgs e) 
        {
            Player.Ахалай_махалай_прогу_запускай(true);
            this.Close();
        }
        private void LoadGame_Click(object sender, RoutedEventArgs e)
        {
            Player.Ахалай_махалай_прогу_запускай(false);
            this.Close();
        } 
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

     
    }
}
