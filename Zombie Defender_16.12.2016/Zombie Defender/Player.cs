﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Zombie_Defender
{
    public delegate void SendStr(string Name, string Num, Label lb);
    public delegate void SendPG(double MaxHp, double CurHp, ProgressBar pg, Label lb);
    public delegate void SendRuneImg(int NumBtn, int QualityRune);
    public delegate void SendImg(Border Img, ImageBrush IB);
    public static class Player
    {
        public static string FolderZD_ 
        {
            get 
            {
                return FolderZD;
            }
        }
        private static string FolderZD = @"c:\ZD";
        public static string RS_
        {
            get
            {
                return RS;
            }
        }
        private static string RS = @"\RS.ZD";
        public static string ZS_
        {
            get
            {
                return ZS;
            }
        }
        private static string ZS = @"\ZS.ZD";
        public static string GS_
        {
            get
            {
                return GS;
            }
        }
        private static string GS = @"\GS.ZD";
        public static string PS_
        {
            get
            {
                return PS;
            }
        }
        private static string PS = @"\PS.ZD";

        public static bool GameIsStarted_
        {
            get 
            {
                return GameIsStarted;
            }
            set 
            {
                GameIsStarted = value;
            }
        }
        private static bool GameIsStarted = false;
        public static bool GameIsNew_
        {
            get
            {
                return GameIsNew;
            }
            set
            {
                GameIsNew = value;
            }
        }
        private static bool GameIsNew;


        public static MainWindow MW_ 
        {
            get 
            {
                return MW;
            }
        }
        private static MainWindow MW;
        private static UpgradeForm UF;

        public static ImageBrush[] WaveImgs = new ImageBrush[4];
        public static  Random rnd = new Random();
        private static Timer Tick;

        public static SendStr SendText;
        public static SendPG SendPG;
        public static SendRuneImg SendRI;
        public static SendImg SendIm;

        private static byte RuneCounter
        {
            get
            {
                return RuneCounter_;
            }
            set
            {
                RuneCounter_ = value;
            }
        }
        private static byte RuneCounter_;
      
        public static Rune[] StorageRune_
        {
            get 
            {
                return StorageRune;
            }
        }
        private static Rune[] StorageRune = new Rune[100];
        private static Goose_Leader Gooses = new Goose_Leader();
        private static Zombie Zombies = new Zombie();

        public static int Level;
        public static double Gall_
        {
            get 
            {
                return Gall;
            }
            set 
            {
                Gall = value;
            }
        }
        private static double Gall;

        private static Stopwatch SW = new Stopwatch();//Для Отладки 
        private static byte SaveCounter = 0;
        private static void Ахалай_Махалай_Бубны_Доставай(object obj) //Main Thread 
        {
            SW.Restart();//Для Отладки 
            SW.Start();//Для Отладки 

            SendText = MW.GiveLb;
            SendPG = MW.GivePG;
            SendIm = MW.GiveCurWave;

            double[] Result = Zombies.TickWave(Gooses.Dps_);

            if (Result[0] == -1) 
            {
                MW.Dispatcher.BeginInvoke(SendIm, MW.WaveBg, WaveImgs[0]);
            }
            else if (Result[0] == 1) 
            {
                MW.Dispatcher.BeginInvoke(SendIm, MW.WaveBg, WaveImgs[1]);
                if(rnd.Next(0, 101) <= rnd.Next(10, 21))
                    CreateRune();
            }
            else if (Result[0] == 2)
            {
                MW.Dispatcher.BeginInvoke(SendIm, MW.WaveBg, WaveImgs[3]);
                CreateRune();
            }
            else if (Result[0] == -2)
            {
                MW.Dispatcher.BeginInvoke(SendIm, MW.WaveBg, WaveImgs[2]);
            }
                

            Gall += Result[1] + Math.Floor((Result[1] / 100) * Gooses.AllProfit_);

           MW.Dispatcher.BeginInvoke(SendText, "Gall: ",  Gall.ToString(), MW.LbGall);
           MW.Dispatcher.BeginInvoke(SendText, "Zombie: ",  Zombies.Count_.ToString(), MW.LbCurZombie);
           MW.Dispatcher.BeginInvoke(SendText, "Wave: " , Level.ToString(), MW.LbWave);
           MW.Dispatcher.BeginInvoke(SendText, "DPS: " , Gooses.Dps_.ToString(), MW.LbDPS);

           MW.Dispatcher.BeginInvoke(
                SendPG, Zombies.GetMaxHp(), Zombies.GetCurHp(), MW.ZombiesHpBar, MW.ZombiesHpBarText);

            if (SaveCounter == 10)
            {
                SaveCounter = 0;
                SaveGame();
            }
            else
                SaveCounter++;

             SW.Stop();  //Для Отладки       
             MW.LbSW.Dispatcher.BeginInvoke(SendText, "", SW.ElapsedTicks.ToString(), MW.LbSW); //Для Отладки  
        } 

        public static string LineSeparation(string str)
        {
            string[] Buff = new string[2] { "", "" };
            for (int i = 0, count = 0; i < str.Length; i++, count++)
                if (count == 3)
                {
                    Buff[0] += " ";
                    Buff[0] += str[str.Length - 1 - i];
                    count = 0;
                }
                else
                    Buff[0] += str[str.Length - 1 - i];

            for (int i = 0; i < Buff[0].Length; i++)
                Buff[1] += Buff[0][Buff[0].Length - 1 - i];
            return Buff[1];
        }

        public static double GetUpdateStat(int GooseNum, int TypeUpdate) 
        {
            return Gooses.UpdateStatsOne(GooseNum, TypeUpdate);
        }
        public static int CurGoose 
        {
            get 
            {
                return MW.curGoose;
            }
        }
        public static byte GooseCount
        {
            get
            {
                return Gooses.GooseCount_;
            }

        }
        public static void BuyGoose() 
        {
            Gall_ -= Gooses.BuyNewGoose();
        }
        public static double CostBuyGoose 
        {
            get 
            {
                return Gooses.CostBuyGoose;
            }
        }

        public static void SetUF(UpgradeForm UF_) 
        {
            UF = UF_;
        }
        public static void CloseUF() 
        {
            UF = null;
        }

        public static void UpdateStats() 
        {
            Gooses.UpdateStats();
        }
        public static int[] UnEquipRune(int CurGoose, int CurRune) 
        {
            Rune rune = Gooses.UnEquipGRune(CurGoose, CurRune);
            int[] RuneInfo = new int[2];

            for (int StorgradeNum = 0; StorgradeNum < 100; StorgradeNum++)
                if (StorageRune[StorgradeNum] == null) 
                {
                    StorageRune[StorgradeNum] = rune;
                    RuneCounter++;
                    RuneInfo[0] = StorgradeNum;
                    RuneInfo[1] = rune.Quality;

                    break;
                }
            return RuneInfo;
            
        }
        public static Rune GetRuneGoose(int TypeRune, int GooseNum) 
        {
            return Gooses.GetRune(TypeRune, GooseNum);
        }
        public static bool GooseExistRune(int TypeRune, int GooseNum) 
        {
            if (Gooses.IsRuneExist(GooseNum, TypeRune))
                return true;
            return false;
        }
        public static bool EquipRune(int GooseNum, int RuneNum) 
        {
            if (Gooses.IsRuneExist(GooseNum, StorageRune[RuneNum].Type) == false) 
            {
                Gooses.EquipRune(StorageRune[RuneNum], GooseNum);
                StorageRune[RuneNum] = null;
                RuneCounter--;
                return true;
            }
            return false;
        }
        public static int[] GrindRune(int num, int AttemptsGrind)
        {
            Gall -= StorageRune[num].CostGrind(AttemptsGrind);
            int[] RusultG = StorageRune[num].ChanceGrind(rnd, AttemptsGrind);
            MW.Dispatcher.BeginInvoke(SendText, "Gall: ", Gall.ToString(), MW.LbGall);
            return RusultG;
        }
        public static void SellRune(int num)
        {
            Gall_ += StorageRune[num].Sell;
            StorageRune[num] = null;
            RuneCounter--;
        }
        public static void SellGRune(int RuneSlot, int CurGoose) 
        {
            Gall_ += Gooses.SellGooseRune((Math.Abs(RuneSlot) - 1), CurGoose);
        }
        private static void CreateRune() 
        {
            if (RuneCounter < 100)
                for (int StorgradeNum = 0; StorgradeNum < 100; StorgradeNum++)
                    if (StorageRune[StorgradeNum] == null)
                    {
                        RuneCounter++;
                        Rune rune = new Rune();
                        rune.Generate(rnd);
                        StorageRune[StorgradeNum] = rune;
                        //UF.SetImageRune(StorgradeNum, StorageRune[StorgradeNum].Quality);
                        if (UF != null) 
                        {
                            SendRI = UF.SetImageRune;
                            MW.Dispatcher.BeginInvoke(SendRI, StorgradeNum, StorageRune[StorgradeNum].Quality);
                        }
                        break;
                    }
        }

        public static void SetMainForm( MainWindow mw)
        {
            MW = mw;
        }
        public static  void Ахалай_Махалай_Работай_Давай(bool IsNewGame)
        { 
           
            if (IsNewGame == true)
                NewGame();
            else
                LoadGame();

            Player.WaveImgs[0] = new ImageBrush(new BitmapImage(new Uri("Resources/CommonWave.png", UriKind.Relative)));
            Player.WaveImgs[1] = new ImageBrush(new BitmapImage(new Uri("Resources/DeadCommonWave.png", UriKind.Relative)));
            Player.WaveImgs[2] = new ImageBrush(new BitmapImage(new Uri("Resources/BossWave.png", UriKind.Relative)));
            Player.WaveImgs[3] = new ImageBrush(new BitmapImage(new Uri("Resources/DeadBoosWave.png", UriKind.Relative)));

            TimerCallback timerCB = new TimerCallback(Ахалай_Махалай_Бубны_Доставай); //поток
            Tick = new Timer(timerCB, null, 0, 1000);
        }

        public static void Ахалай_махалай_прогу_запускай(bool IsNewGame)
        {
            GameIsStarted_ = true;
            MW.NewGame(IsNewGame);
        }
        private static void NewGame()
        {
            RuneCounter = 0;
            Level = 1;
            Gall = 0;

            Zombies.NewGame();
            Gooses.NewGame();
        }
        private static void LoadGame()
        {
            FileStream fs = File.OpenRead(FolderZD + RS);
            StreamReader sr = new StreamReader(fs, System.Text.Encoding.Default);
            string Str = "";


            RuneCounter = Byte.Parse(sr.ReadLine().Split('&')[1].Split('=')[1]);
            for (int i = 0; i < RuneCounter; i++)
            {
                Str = sr.ReadLine();

                Rune rune = new Rune();
                rune.Type = Byte.Parse(Str.Split('&')[1].Split('=')[1]);
                rune.Quality = Byte.Parse(Str.Split('&')[2].Split('=')[1]);
                rune.MainStat = Int32.Parse(Str.Split('&')[3].Split('=')[1]);
                rune.Grind = Int32.Parse(Str.Split('&')[4].Split('=')[1]);
                StorageRune[i] = rune;
            }   

            fs.Close();
            sr.Close();


            fs = File.OpenRead(FolderZD + PS);
            sr = new StreamReader(fs, System.Text.Encoding.Default);
            Str = sr.ReadLine();
            Level = Int32.Parse(Str.Split('&')[1].Split('=')[1]);
            Gall = Double.Parse(Str.Split('&')[2].Split('=')[1]);

            fs.Close();
            sr.Close();


            fs = File.OpenRead(FolderZD + ZS);
            sr = new StreamReader(fs, System.Text.Encoding.Default);

            int Count;
            double HpBoss;
            Str = sr.ReadLine();
            if (Str.Split('&')[1].Split('=')[1] == "false")
                HpBoss = -1;
            else
                HpBoss = double.Parse(Str.Split('&')[1].Split('=')[1]);
            Count = Int32.Parse(Str.Split('&')[2].Split('=')[1]);
            Zombies.Loadsave(HpBoss, Count);

            fs.Close();
            sr.Close();


            fs = File.OpenRead(FolderZD + GS);
            sr = new StreamReader(fs, System.Text.Encoding.Default);
            if (File.Exists(FolderZD + GS) == true)
                Gooses.LoadSave(sr);

            fs.Close();
            sr.Close();
            
        }
        private static void SaveGame()
        {
            if (Directory.Exists(FolderZD) == false)
                Directory.CreateDirectory(FolderZD);

            FileStream fs;

            fs = File.Create(FolderZD + RS);
            byte[] info;
            string TextLine = "";

            TextLine += "&=" + RuneCounter + "&" + Environment.NewLine;
            info = new UTF8Encoding(true).GetBytes(TextLine);
            fs.Write(info, 0, info.Length);
            for (int i = 0; i < 100; i++)
                if (StorageRune[i] != null) 
                {
                    TextLine = "&=" + StorageRune[i].Type
                                + "&=" + StorageRune[i].Quality
                                + "&=" + StorageRune[i].MainStat
                                + "&=" + StorageRune[i].Grind + "&"
                                + Environment.NewLine;
                    info = new UTF8Encoding(true).GetBytes(TextLine);
                    fs.Write(info, 0, info.Length);
                }
            fs.Close();

            fs = File.Create(FolderZD + PS);
            info = new UTF8Encoding(true).GetBytes("&=" + Level + "&=" + Gall + "&");
            fs.Write(info, 0, info.Length);
            fs.Close();

            fs = File.Create(FolderZD + ZS);
            info = new UTF8Encoding(true).GetBytes(Zombies.GetSave());
            fs.Write(info, 0, info.Length);
            fs.Close();

            fs = File.Create(FolderZD + GS);
            info = new UTF8Encoding(true).GetBytes(Gooses.GetSave());
            fs.Write(info, 0, info.Length);
            fs.Close();
        }
    }
}